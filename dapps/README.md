# Examples that work with the current compiler

These examples demonstrate how to do things with the language as it is
currently implemented, and double as test vectors for the compiler passes.

For examples of the language as it is intended and envisioned see
the [futures](../futures/) directory.

## Tests for various compiler passes

These tests are not particularly meant to implement code for users,
but to stress the capabilities of the compiler:

[passdata/fun.sexp](passdata/fun.sexp),
[passdata/pattern_match.sexp](passdata/pattern_match.sexp),
[passdata/type_defs.sexp](passdata/type_defs.sexp),
[passdata/type_infer.sexp](passdata/type_infer.sexp).

## Rock, Paper, Scissors

[rps.sexp](rps.sexp)

## Buying (and selling) signatures.

[buy_sig.sexp](buy_sig.sexp)

