# Glow

MuKn Glow is a language to write safe Decentralized Applications (DApps)
interacting with cryptocurrency blockchains.
It is designed to minimize the effort required to trust
that a DApp is indeed safe for people to use.

### Copyright and License

Copyright 2019 Mutual Knowledge Systems, Inc. All rights reserved.
Glow is distributed under the Apache License, version 2.0. See the file [LICENSE](LICENSE).

### Installation instructions

See [INSTALL.md](INSTALL.md) for installation instructions.

### Being worked on

You can watch on our [gitlab repository](https://gitlab.com/mukn/glow)
what we are currently working on.

### More information

For more information on Glow, see our [bibliography](docs/bibliography.md).
